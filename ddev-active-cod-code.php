<?php

/*
Plugin name: active COD Code
description: Plugin Kích Hoạt COD, Nếu Bạn Cần ở Khóa Domain Facebook, Kháng Spam Hãy Liên hệ tôi Hotline/Zalo : 0913642029
Author: Ddev
Author URI: https://webdepnhanh.com/website-bi-chan-tren-facebooknguyen-nhan-va-huong-dan-cach-mo-khoa-moi-nhat/
Version: 3.0

*/

//Load javascript fuction
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}
function myloadscripts(){
	wp_register_script('custom_js',plugin_dir_url(__FILE__) .'customscripts.js');
	wp_enqueue_script('custom_js');
	
}
add_action('admin_enqueue_scripts','myloadscripts');
//end load
// If this file is called directly, abort.
//menu admin

if ( ! defined( 'WPINC' ) ) {
     die;
}
 
// Include the dependencies needed to instantiate the plugin.
foreach ( glob( plugin_dir_path( __FILE__ ) . 'admin/*.php' ) as $file ) {
    include_once $file;
}
 
add_action( 'plugins_loaded', 'ddev_custom_admin_settings' );
/**
 * Starts the plugin.
 *
 * @since 1.0.0
 */
function ddev_custom_admin_settings() {
 
    $plugin = new Submenu( new Submenu_Page() );
    $plugin->init();
 }
 
add_action( 'add_meta_boxes', 'mv_add_meta_boxes' );
if ( ! function_exists( 'mv_add_meta_boxes' ) )
{
    function mv_add_meta_boxes()
    {

        add_meta_box( 'mv_other_fields', __('Mã Code kích hoạt','woocommerce'), 'mv_add_other_fields_for_packaging', 'shop_order', 'side', 'core' );

    }
}
// Adding Meta field in the meta container admin shop_order pages
if ( ! function_exists( 'mv_add_other_fields_for_packaging' ) )
{
    function mv_add_other_fields_for_packaging()
    {
        global $post;

        $meta_field_data = get_post_meta( $post->ID, '_ma_kich_hoat', true ) ? get_post_meta( $post->ID, '_ma_kich_hoat', true ) : '';	


        echo '<input  type="hidden" name="mv_other_meta_field_nonce" value="' . wp_create_nonce() . '">
        <p style="border-bottom:solid 1px #eee;padding-bottom:13px;">
            <input id="activecode" type="text" style="width:250px;";" name="my_field_name" placeholder="' . $meta_field_data . '" value="' . $meta_field_data . '"></p>';
        
 		echo '<button onclick="return randomnumber();"> Tạo Mã Kích Hoạt </button>';

    }
}
// Save the data of the Meta field
add_action( 'save_post', 'mv_save_wc_order_other_fields', 10, 1 );
if ( ! function_exists( 'mv_save_wc_order_other_fields' ) )
{

    function mv_save_wc_order_other_fields( $post_id ) {

        // We need to verify this with the proper authorization (security stuff).

        // Check if our nonce is set.
        if ( ! isset( $_POST[ 'mv_other_meta_field_nonce' ] ) ) {
            return $post_id;
        }
        $nonce = $_POST[ 'mv_other_meta_field_nonce' ];

        //Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce ) ) {
            return $post_id;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }

        // Check the user's permissions.
        if ( 'page' == $_POST[ 'post_type' ] ) {

            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {

            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }
        // --- Its safe for us to save the data ! --- //

        // Sanitize user input  and update the meta field in the database.
        update_post_meta( $post_id, '_ma_kich_hoat', $_POST[ 'my_field_name' ] );

    }
}
// Make shorte code insert form on frontend.

add_shortcode( 'insertactivecode', 'active_code' );
function active_code(){
	global $inform;
    check_query();
	echo '<form method="post">';
	echo '<label for="active_code">Nhập Mã Kích Hoạt: </label>';
	echo '<input type="text" id="active_code" name="active_code" /> ';
	echo  '<label for="id-order">Nhập Mã Đơn Hàng: </label>';
	echo '<input type="text" id="id_order" name="id_order"/> ';
	wp_nonce_field( 'wpnonce1','wpnone1protect');
	echo '<input type="submit" id="activecodesubmit" name="activecodesubmit" value="Kích Hoạt" />';
	echo '</form>';
	echo $inform;
	

}
//Check Query match with active code and order id in postmeta
function check_query(){
	global $inform;
	global $wpdb;
	
	
	if(!$_POST['wpnone1protect'] || !wp_verify_nonce( $_POST['wpnone1protect'], 'wpnonce1' ) && !$_POST['activecodesubmit'] ){
		return;
	}
	if($_POST['active_code'] =="" || $_POST['id_order']==""){
		return;
	}
	
	else {
		
	$activecode = $_POST['active_code'];
	$idorder = $_POST['id_order'];
	
	//checking after press checking order
	$countid = $wpdb->get_var($wpdb->prepare("SELECT Count(*) from {$wpdb->base_prefix}woocommerce_order_items WHERE order_id = %d", $idorder));
	$countactive = $wpdb->get_var($wpdb->prepare("SELECT count(*) from $wpdb->postmeta WHERE meta_value = %d", $activecode));
		if ($countid <> "0"&& $contactive <> "0") {
			// check the order completed or not
			$wcorderstatus= $wpdb->get_var($wpdb->prepare("SELECT status from {$wpdb->base_prefix}wc_order_stats where order_id=%d",$idorder));
			if ($wcorderstatus != null){

				switch ($wcorderstatus) {
					Case "wc-completed":
					 return $inform ="Đơn hàng bạn đã được kích hoạt vui lòng kiểm tra email";
					 break;
					default:
					// save complete on database
					// $wpdb->update('{$wpdb->base_prefix}wc_order_stats',
					// 	array(
					// 		'status' =>"wc-completed"	

					// 	),
					// 	array(
					// 		'order_id'=>$idorder
					// 	),
					// 	array( 
					// 		'%s'	
					// 	), 
					// 	array( 
					// 		'%d' ) 

					// );
					//use wc hook
					$updatestatus = new wc_order($idorder);
					$updatestatus->update_status('wc-completed','Active by user - COD code');
					return $inform ="Đơn hàng bạn đã được kích hoạt. Một Email Chứng Thông Tin Vừa Gửi Tới Bạn";

				}
			}
		}
		else {
			return $inform = "wrong order id or active code" ;
		}
		
		}

	
}


require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/lightd/ddev-active-code-vn',
	__FILE__,
	'ddev-active-cod-code'
);
$myUpdateChecker->setBranch	('master');	
$myUpdateChecker->setAuthentication('5RxXjzS1n3hUoKT5-oay');




	
	

?>